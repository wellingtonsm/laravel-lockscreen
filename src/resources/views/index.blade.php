@extends('lockscreen::layouts.master')

@section('content')
    @include('lockscreen::themes.' . config('lockscreen.theme'))
@endsection