<style>
    body {
        background-color: #eee;
        font-family: 'Roboto', sans-serif;
    }

    .flex-container {
        padding: 20px;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -ms-flex-align: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        align-items: center;
    }

    .flex-container > * {
        margin: auto;
    }

    .box-lockscreen {
        min-width: 280px;
        max-width: 400px;
        height: auto;
        background-color: rgba(255, 255, 255, .9);
        border: 1px solid #d1d4d7;
        padding: 30px;
    }

    h2 {
        font-size: 16px;
        line-height: 16px;
        font-weight: 400;
        text-align: center;
    }

    p {
        margin-top: -8px;
        font-weight: 300;
        text-align: center;
    }

    .group {
        position: relative;
        margin-bottom: 10px;
        margin-top: 20px;
    }

    input {
        -moz-appearance: none;
        -webkit-appearance: none;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border: 1px solid transparent;
        -webkit-border-radius: 290486px;
        -moz-border-radius: 290486px;
        border-radius: 290486px;
        -webkit-box-shadow: none;
        box-shadow: none;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        font-size: 1rem;
        height: 2.25em;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        line-height: 1.5;
        padding-bottom: calc(0.375em - 1px);
        padding-left: calc(0.625em + 5px);
        padding-right: calc(0.625em - 1px);
        padding-top: calc(0.375em - 1px);
        position: relative;
        vertical-align: top;
        background-color: white;
        border-color: #dbdbdb;
        color: #363636;
        -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);
        box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);
        max-width: 100%;
        width: 100%;
    }

    input:focus {
        outline: none;
    }

    .btn {
        -moz-appearance: none;
        -webkit-appearance: none;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border: 1px solid transparent;
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        display: block;
        font-size: 1rem;
        height: 2.25em;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        line-height: 1.5;
        padding-bottom: calc(0.375em - 1px);
        padding-left: calc(0.625em - 1px);
        padding-right: calc(0.625em - 1px);
        padding-top: calc(0.375em - 1px);
        position: relative;
        vertical-align: top;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: white;
        border-color: #dbdbdb;
        color: #363636;
        cursor: pointer;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        padding-left: 0.75em;
        padding-right: 0.75em;
        text-align: center;
        white-space: nowrap;
        -webkit-border-radius: 290486px;
        -moz-border-radius: 290486px;
        border-radius: 290486px;
        padding-left: 3em;
        padding-right: 3em;
        margin: 0 auto;
    }

    .btn{
        background-color: #3273dc;
        border-color: transparent;
        color: #fff;
    }

    .btn:hover{
        background-color: #276cda;
        border-color: transparent;
        color: #fff;
    }

    .btn:focus, .btn:active {
        outline: none;
    }

    a {
        display: block;
        font-weight: 500;
        font-size: 14px;
        color: #337ab7;
        text-decoration: none;
        -webkit-tap-highlight-color: transparent;
        text-align: center;
    }
</style>