<style>
    body {
        background-color: #eee;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    .flex-container {
        padding: 20px;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -ms-flex-align: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        align-items: center;
    }

    .flex-container > * {
        margin: auto;
    }

    .box-lockscreen {
        min-width: 280px;
        max-width: 400px;
        height: auto;
        background-color: rgba(255, 255, 255, .9);
        border: 1px solid #d1d4d7;
        padding: 30px;
    }

    h2 {
        font-size: 16px;
        line-height: 16px;
        font-weight: 400;
    }

    p {
        margin-top: -8px;
        font-weight: 300;
    }

    .input-group {
        position: relative;
        display: flex;
        border-collapse: separate;
    }

    .input-group .form-control, .input-group-btn {
        display: table-cell;
    }

    .input-group .form-control {
        position: relative;
        z-index: 2;
        float: left;
        width: 100%;
        margin-bottom: 0;
        border: 1px solid #d1d4d7;
        color: #3d3f42;
    }

    .form-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }

    .input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group {
        margin-left: -1px;
    }

    .input-group-btn > .btn {
        position: relative;
        margin-bottom: 25px;
        color: #fff;
        background: #337ab7;
        border: 1px solid #337ab7;
        text-shadow: none;
    }

    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
    }

    a {
        display: block;
        font-weight: 300;
        font-size: 14px;
        color: #337ab7;
        text-decoration: none;
    }
</style>