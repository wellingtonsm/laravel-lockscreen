<div class="box-lockscreen">
    <h2><strong>{{ Auth::user()->name }}</strong></h2>
    <p>{{ Auth::user()->email }}</p>

    <form action="{{ route('lockscreen.unlock') }}" method="POST">
        {{ csrf_field() }}

        <div class="group">
            <input type="password" name="password" required>
            <span class="bar"></span>
            <label>Password</label>
        </div>

        <button class="btn">Unlock</button>
    </form>

    <a href="javascript:void(0)" id="lockscreen-logout">I'm not {{ Auth::user()->name }}</a>
</div>