<div class="box-lockscreen">
    <h2><strong>{{ Auth::user()->name }}</strong></h2>
    <p>{{ Auth::user()->email }}</p>

    <form action="{{ route('lockscreen.unlock') }}" method="POST">
        {{ csrf_field() }}

        <div class="input-group">
            <input class="form-control" type="password" name="password" placeholder="Password" required>
            <span class="input-group-btn">
                <button class="btn">Unlock</button>
            </span>
        </div>
    </form>

    <a href="javascript:void(0)" id="lockscreen-logout">I'm not {{ Auth::user()->name }}</a>
</div>