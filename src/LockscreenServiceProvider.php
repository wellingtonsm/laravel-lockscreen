<?php

namespace Wellingtonsm\Lockscreen;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class LockscreenServiceProvider extends ServiceProvider
{
	/**
	 * All of the short-hand keys for middlewares.
	 *
	 * @var array
	 */
	protected $middleware = [
		'auth.lock' => \Wellingtonsm\Lockscreen\Middleware\CheckIsLockSession::class
	];

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot(Router $router)
	{
		$this->publishes([
			__DIR__ . '/config/lockscreen.php' => config_path('lockscreen.php')
		]);

		$this->mergeConfigFrom(__DIR__ . '/config/lockscreen.php', 'lockscreen');

		foreach ($this->middleware as $name => $class) {
			$router->pushMiddlewareToGroup('web', $class);
		}

		$this->loadRoutesFrom(__DIR__ . '/routes/web.php');
		$this->loadViewsFrom(__DIR__ . '/resources/views', 'lockscreen');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->make('Wellingtonsm\Lockscreen\Controllers\LockscreenController');

	}
}
