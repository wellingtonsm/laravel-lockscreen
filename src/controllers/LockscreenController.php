<?php

namespace Wellingtonsm\Lockscreen\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Wellingtonsm\Lockscreen\Services\LockscreenService;
use Illuminate\Support\Facades\Auth;

class LockscreenController extends Controller
{
	protected $lockscreen_service;

	public function __construct(LockscreenService $lockscreen_service)
	{
		$this->lockscreen_service = $lockscreen_service;

		$this->middleware('web');
	}

	/**
	 * Lock session and display the lockscreen.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function lock()
	{
	    if(Auth::guest()) return redirect()->back();

		$lock = $this->lockscreen_service->lock();

		if($lock['status']) return view('lockscreen::index', compact('lock'));

		return redirect($lock['page']);
	}

	/**
	 * Try unlock the session by password.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function unlock(Request $request)
	{
		$result = $this->lockscreen_service->lockOrUnlock($request);

	    return redirect($result['page']);
	}
}
