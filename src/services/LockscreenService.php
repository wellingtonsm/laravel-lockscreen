<?php

namespace Wellingtonsm\Lockscreen\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class LockscreenService
{
	public function getLock()
	{
		return session()->get('lock', [
			'password' => '',
			'status' => false,
			'page' => ''
		]);
	}

	public function lockOrUnlock(Request $request)
	{
		$lock = $this->getLock();

		if ($lock['status']) return $this->unlock($request, $lock);

		return $this->lock();
	}

	public function lock()
	{
		session()->put('lock', [
			'password' => Auth::user()->getAuthPassword(),
			'status' => true,
			'page' => URL::previous()
		]);

		return $this->getLock();
	}

	public function unlock(Request $request, $lock)
	{
		$attributes = $request->all();

		if (Hash::check($attributes['password'], $lock['password'])) {
			$lock['status'] = false;

			session()->put('lock', $lock);
		}

		return $lock;
	}
}
