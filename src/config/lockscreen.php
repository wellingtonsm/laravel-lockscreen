<?php

return [
	/*
    |--------------------------------------------------------------------------
    | View Theme
    |--------------------------------------------------------------------------
    |
    | Here you choose your view theme for the lockscreen
    |
    | Available Themes: "adminlte", "bootstrap", "materialize", "bulma"
    |
    */
	'theme' => 'adminlte'
];