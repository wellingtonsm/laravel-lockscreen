<?php

namespace Wellingtonsm\Lockscreen\Middleware;

use Wellingtonsm\Lockscreen\Services\LockscreenService;
use Closure;

class CheckIsLockSession
{
	protected $lockscreen_service;

	public function __construct(LockscreenService $lockscreen_service)
	{
		$this->lockscreen_service = $lockscreen_service;
	}
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$lock = $this->lockscreen_service->getLock();

    	if($lock['status'] && !str_contains($request->route()->getName(), 'lockscreen')){
    		return redirect()->route('lockscreen.lock');
	    }

        return $next($request);
    }
}
