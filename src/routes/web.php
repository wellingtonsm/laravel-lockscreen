<?php

Route::group(['namespace' => 'Wellingtonsm\\Lockscreen\\Controllers', 'as' => 'lockscreen.'], function (){
	Route::get('lock', 'LockscreenController@lock')->name('lock');
	Route::post('unlock', 'LockscreenController@unlock')->name('unlock');
});
